import React from 'react';
import './app.scss';
import block from 'bem-cn';
import {Modal} from "./components";
import {ModalContext, useModalContext} from "./context/ModalContext/ModalContext";
import Registration from "./modules/Auth/Registration/registration";
import Signin from "./modules/Auth/SignIn/signin";

const b = block('app');

function App() {
  const { modal, openModal, closeModal } = useModalContext();
  return (
    <ModalContext.Provider value={{ modal, openModal, closeModal }}>
      <div className={b()}>
        <button
          onClick={() => {
             openModal(<Registration />)
          }}
        >
          Зарегистрироваться
        </button>
        <button
          onClick={() => {
            openModal(<Signin />)
          }}
        >
          Войти
        </button>
      </div>
      <Modal />
    </ModalContext.Provider>
  );
}

export default App;
