import React, {useContext, useState} from "react";

export interface IModalContext {
  modal: React.ReactNode | null;
  openModal: (content: JSX.Element) => void;
  closeModal: () => void;
}

export const ModalContext = React.createContext<IModalContext>({} as IModalContext);

export const useModalContext = () => {
  const [modal, setModal] = useState<React.ReactNode | null>(null);
  const closeModal = () => {
    setModal(null);
  };
  const openModal = (content: (JSX.Element)) => {
    setModal(null);
    setTimeout(() => {
      setModal(content);
    }, 100);
  }
  return { modal, closeModal, openModal };
};

export const useModal = () => {
  const { modal, closeModal, openModal } = useContext(ModalContext);

  return { modal, closeModal, openModal };
};