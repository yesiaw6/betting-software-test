export const getOptionsDays = () => {
  return [...new Array(31)].map((el, idx) => {
    return { value: idx, label: `${idx + 1}` }
  });
};

export const getOptionsMonths = () => {
  const months = ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек']
  return months.map((el, idx) => {
    return { value: idx, label: el }
  });
};

export const getOptionsYears = () => {
  const initialYear = new Date().getFullYear();
  return [...new Array(100)].map((el, idx) => {
    return { value: initialYear - idx, label: `${initialYear - idx}` }
  });
};
