import {getOptionsDays, getOptionsMonths, getOptionsYears} from "./helpers";
import {IOption} from "../components/interfaces";

export const OPTIONS_DAYS: IOption[] = getOptionsDays();
export const OPTIONS_MONTH: IOption[] = getOptionsMonths();
export const OPTIONS_YEARS: IOption[] = getOptionsYears();
export const OPTIONS_GENDERS: IOption[] = [
  {
    value: 1,
    label: 'Мужской',
  },
  {
    value: 2,
    label: 'Женский',
  },
  {
    value: 3,
    label: 'Другое',
  },
];