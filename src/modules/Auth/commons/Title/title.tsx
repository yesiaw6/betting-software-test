import React, {FC} from 'react';
import block from 'bem-cn';

import './title.scss';

export interface ITitle {
  title: string;
  subtitle: string;
}

const b = block('title');
const Title: FC<ITitle> = ({ title, subtitle }) => {
  return (
    <div className={b('container')}>
      <p className={b('text')}>{title}</p>
      <p className={b('subtitle')}>{subtitle}</p>
      <div className={b('line')} />
    </div>
  );
};

export default Title;

