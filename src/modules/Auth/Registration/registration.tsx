import React from 'react';
import block from 'bem-cn';

import './registration.scss';
import {BasicInfo, BirthDate, Gender} from "./components";
import Title from "../commons/Title/title";
import {useModal} from "../../../context/ModalContext/ModalContext";
import Signin from "../SignIn/signin";
import {Formik} from "formik";
import {IInitialValuesRegistration} from "../interfaces";

const b = block('registration');
const Registration = () => {
  const { openModal } = useModal();
  return (
    <div className={b()}>
      <Title title="Регистрация" subtitle="Быстро и легко." />
      <Formik
        validateOnChange={false}
        validateOnMount={false}
        validateOnBlur={false}
        initialValues={{
          name: '',
          last_name: '',
          email_phone: '',
          password: '',
          day: '1',
          month: 'янв',
          year: '2022',
          gender: '',
      }}
        validate={(values: IInitialValuesRegistration) => {
          const keys = Object.keys(values);
          return keys.reduce((acc, cur: string) => {
            if (values[cur].length > 0) {
              return acc;
            }
            return {...acc, [cur]: 'Это обязательное поле'};
          }, {});
        }}
        onSubmit={(values) => {
          alert(JSON.stringify(values, null, 2));
        }}
      >
        {props => (
          <form className={b('form')} onSubmit={props.handleSubmit}>
            <BasicInfo />
            <BirthDate />
            <Gender />
            <button
              type="submit"
              className={b('button_submit')}
            >
              Регистрация
            </button>
          </form>
        )}
      </Formik>
      <button
        type="button"
        onClick={() => openModal(<Signin />)}
        className={b('button_signin')}
      >
        У вас уже есть аккаунт?
      </button>
    </div>
  );
};

export default Registration;

