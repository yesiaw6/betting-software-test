import React, {FC} from 'react';
import block from 'bem-cn';

import './birthDate.scss';
import {Select} from "../../../../../components";
import {OPTIONS_DAYS, OPTIONS_MONTH, OPTIONS_YEARS} from "../../../../../utils/constants";
import {useFormikContext} from "formik";

const b = block('birthDate');

const BirthDate: FC = () => {
  const { handleChange } = useFormikContext();

  return (
    <div className={b()}>
      <p className={b('title')}>Дата рождения</p>
      <div className={b('selects')}>
        <Select name="day" onChange={handleChange} options={OPTIONS_DAYS} />
        <Select name="month" onChange={handleChange} options={OPTIONS_MONTH} />
        <Select name="year" onChange={handleChange} options={OPTIONS_YEARS} />
      </div>
    </div>
  );
};

export default BirthDate;

