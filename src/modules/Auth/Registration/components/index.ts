import BasicInfo from "./BasicInfo/basicInfo";
import BirthDate from "./BirthDate/birthDate";
import Gender from "./Gender/gender";

export { BasicInfo, BirthDate, Gender };
