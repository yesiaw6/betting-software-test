import React, {FC} from 'react';
import block from 'bem-cn';

import './basicInfo.scss';
import {Input} from "../../../../../components";
import {useFormikContext} from "formik";
import {IFormikErrors} from "../../../interfaces";

const b = block('basicInfo');

const BasicInfo: FC = () => {
  const { handleChange, errors } = useFormikContext<IFormikErrors>();
  return (
    <div className={b()}>
      <div className={b('fio')}>
        <Input
          onChange={handleChange}
          name="name"
          id="name"
          placeholder="Имя"
          error={errors.name}
        />
        <Input
          onChange={handleChange}
          name="last_name"
          id="last_name"
          placeholder="Фамилия"
          error={errors.last_name}
        />
      </div>
      <Input
        onChange={handleChange}
        name="email_phone"
        id="email_phone"
        placeholder="Номер мобильного телефона или эл. адрес"
        error={errors.email_phone}
      />
      <Input
        onChange={handleChange}
        name="password"
        id="password"
        placeholder="Новый пароль"
        type="password"
        error={errors.password}
      />
    </div>
  );
};

export default BasicInfo;

