import React from 'react';
import block from 'bem-cn';

import './gender.scss';
import {OPTIONS_GENDERS} from "../../../../../utils/constants";
import {RadioInput} from "../../../../../components";
import {useFormikContext} from "formik";
import {IFormikErrors} from "../../../interfaces";

const b = block('gender');


const Gender = () => {
  const { handleChange, values, errors } = useFormikContext<IFormikErrors>();

  return (
    <div className={b()}>
      <p className={b('title')}>Пол</p>
      <div className={b('radios')}>
        {OPTIONS_GENDERS.map((option) => (
          <RadioInput
            key={option.value}
            name="gender"
            value={option.value}
            label={option.label}
            onChange={handleChange}
            pickedValue={values.gender}
          />
        ))}
      </div>
      {errors.gender && (
        <div className={b('validation')}>Выберите пол</div>
      )}
    </div>
  );
};

export default Gender;

