export interface IInitialValuesRegistration {
  name: string,
  last_name: string,
  email_phone: string,
  password: string,
  day: string,
  month: string,
  year: string,
  gender: string,
  [key: string]: string;
}

type Optional<T> = { [K in keyof T]?: T[K] }

export type IFormikErrors = Optional<IInitialValuesRegistration>;

export interface IInitialValuesSignin {
  email_phone: string,
  password: string,
  [key: string]: string;
}