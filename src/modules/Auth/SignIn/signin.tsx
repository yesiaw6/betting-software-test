import React, {FC} from 'react';
import block from 'bem-cn';

import './signin.scss';
import Title from "../commons/Title/title";
import {Input} from "../../../components";
import {useModal} from "../../../context/ModalContext/ModalContext";
import Registration from "../Registration/registration";
import {Formik} from "formik";
import {IInitialValuesSignin} from "../interfaces";

const b = block('signin');

const Signin: FC = () => {
  const { openModal } = useModal();
  return (
    <div className={b()}>
      <Title title="Уже есть аккаунт?" subtitle="Войдите" />
        <Formik
          validateOnChange={false}
          initialValues={{
            email_phone: '',
            password: '',
          }}
          validate={(values: IInitialValuesSignin) => {
            const keys = Object.keys(values);
            return keys.reduce((acc, cur: string) => {
              if (values[cur].length > 0) {
                return acc;
              }
              return {...acc, [cur]: 'Это обязательное поле'};
            }, {});
          }}
          onSubmit={(values) => {
            alert(JSON.stringify(values, null, 2));
          }}
        >
          {props => (
            <form className={b('inputs')} onSubmit={props.handleSubmit}>
              <Input
                placeholder="эл. адрес или номер телефона"
                name="email_phone"
                id="email_phone"
                onChange={props.handleChange}
                error={props.errors.email_phone}
              />
              <Input
                placeholder="Пароль"
                name="password"
                id="password"
                type="password"
                onChange={props.handleChange}
                error={props.errors.password}
              />
              <button
                type="submit"
                className={b('button_submit')}
              >
                Войти
              </button>
            </form>
          )}
        </Formik>
      <button
        type="button"
        onClick={() => openModal(<Registration />)}
        className={b('button_registration')}
      >
        Регистрация
      </button>
    </div>
  );
};

export default Signin;

