import React, {FC, useEffect, useState} from 'react';
import block from 'bem-cn';
import './modal.scss';
import {useModal} from "../../context/ModalContext/ModalContext";

const b = block('modal');

const Modal: FC = () => {
  const { modal, closeModal } = useModal();
  const [localActive, setLocalActive] = useState(!!modal);
  useEffect(() => {
    if (!modal) {
      setLocalActive(!!modal);
    } else {
      setTimeout(() => {
        setLocalActive(!!modal);
      }, 100)
    }
  }, [modal]);
  if (modal) {
    return (
      <div className={b('container')}>
        <div
          role="button"
          tabIndex={0}
          className={b('background')}
          onClick={closeModal}
        />
        <div className={b('content', { active: localActive })}>
          {modal}
        </div>
      </div>
    );
  }
  return null;
};

export default Modal;