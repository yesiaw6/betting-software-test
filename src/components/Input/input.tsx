import React, {FC, useState} from 'react';
import block from 'bem-cn';

import './input.scss';
import {IInputProps} from "../interfaces";

const b = block('input');

const Input: FC<IInputProps> = ({ value, onChange, placeholder, error, name, id, type = 'text' }) => {
  const [localValue, setLocalValue] = useState('');
  const handleChange = (event: React.ChangeEvent) => {
    const target = event.target as HTMLInputElement;
    setLocalValue(target.value);
    if (onChange) {
      onChange(event);
    }
  };
  const inputValue = onChange ? value : localValue;
  return (
    <div>
      <input
        name={name}
        id={id}
        value={inputValue}
        onChange={handleChange}
        placeholder={placeholder}
        className={b({ error: !!error })}
        type={type}
      />
      {error && <p>{error}</p>}
    </div>
  );
};

export default Input;
