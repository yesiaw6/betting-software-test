import React from "react";

export interface IOption {
  value: number;
  label: string;
}

export interface IDefaultInput {
  value?: string | number;
  onChange?: ((event: React.SetStateAction<string | number | undefined> | React.ChangeEvent | React.ChangeEvent<HTMLSelectElement>) => void) | undefined;
}

export interface IInputProps extends IDefaultInput {
  placeholder?: string;
  error?: string;
  name?: string;
  id?: string;
  type?: string;
}

export interface ISelectProps extends IDefaultInput {
  options?: IOption[];
  name?: string;
}
export interface IRadioProps extends ISelectProps {
  label?: string;
  name: string;
  pickedValue?: number | string | undefined;
}
