import React, {FC} from 'react';
import block from 'bem-cn';

import './select.scss';
import {ISelectProps} from "../interfaces";
import {Field} from "formik";

const b = block('select');

const Select: FC<ISelectProps> = ({ options, onChange, name }) => {
  return (
    <Field className={b()} onChange={onChange} as="select" name={name}>
      {options?.map((option) => (
        <option key={option.value}>{option.label}</option>
      ))}
    </Field>
  );
};

export default Select;

