import React, {FC} from 'react';
import block from 'bem-cn';

import './radioInput.scss';
import {IRadioProps} from "../interfaces";
import {Field} from "formik";

const b = block('radio');

const RadioInput: FC<IRadioProps> = ({ value, label, name, pickedValue }) => {
  const checked = pickedValue == value;
  return (
    <label className={b('label')}>
      <p>{label}</p>
      <Field
        type="radio"
        name={name}
        value={value}
        checked={checked}
      />
    </label>
  );
};

export default RadioInput;

